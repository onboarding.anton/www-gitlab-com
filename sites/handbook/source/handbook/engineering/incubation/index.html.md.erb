---
layout: handbook-page-toc
title: Incubation Engineering Department
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Incubation Engineering Department

The Incubation Engineering Department within the Engineering Division focuses on projects that are pre-Product/Market fit. The projects they work on align with the term "new markets" from one of our [product investment types](/handbook/product/investment/#investment-types). They are ideas that may contribute to our revenue in 3-5 years time. Their focus should be to move fast, ship, get feedback, and [iterate](/handbook/values/#iteration). But first they've got to get from 0 to 1 and get something shipped.

We utilize [Single-engineer Groups](/company/team/structure/#single-engineer-groups) to draw benefits, but not distractions, from within the larger company and [GitLab project](https://gitlab.com/gitlab-org/gitlab) to maintain that focus. The Single-engineer group encompasses all of product development (product management, engineering, design, and quality) at the smallest scale. They are free to learn from, and collaborate with, those larger departments at GitLab but not at the expense of slowing down unnecessarily.

The Department Head is the [VP of Incubation Engineering](/job-families/engineering/vp-of-incubation-engineering/).

### FY22 Direction

The Incubation Engineering Department is new, and the focus for FY22 is to:

* Hire team members that work as Single-Engineer Groups to deliver the departments priorities.
* Develop a repeatable process and appropriate performance indicators that allow the team to measure and demonstrate their progress and impact for each of the areas we are interested in delivering.
* Develop a scoring framework and methodology to accept and prioritise new ideas.

### Investment Framework

The aim of the SEG's in the Incubation Engineering Department is to get ideas for new markets into the Viable category from our [maturity](https://about.gitlab.com/direction/maturity/) framework.  Ideas that we successfully incubate will become a stage or feature within the relevant areas of our product, with full Product, Engineering, UX and Infrastructure support for future development.

In some cases we may invest in an SEG and then decide to acquire an existing product or team in the same area. These situations are disruptive, but unavoidable, and we will work closely with the affected SEG to adapt.

In order to determine our investment into an SEG, we use the following assumptions:

* SEGs have a 25% success rate (as opposed to 5% of VC funded companies), due to the existing market reach and technical foundations of the GitLab product and the culture, processes and support from the wider organisation.
* SEG's mature categories at a 50% faster rate than regular GitLab Category Maturity features, due to the narrow focus of our investments.
* SEG's ramp up revenue as good as the best startups in the world (5 year to $100M ARR)

Reference:

* <https://kimchihill.com/2020/04/09/path-to-100m-arr/>
* <https://www.stephnass.com/blog/saas-ipo-roadmap>

### Demonstrating Progress

Our key focus is to ship software, we do this [Iteratively](https://about.gitlab.com/handbook/values/#iteration) and [Transparently](https://about.gitlab.com/handbook/values/#transparency) in an Agile way, concentrating on having something that works as soon as possible that we can build upon.

For each of our Investment areas, we aim to showcase our progress on a weekly basis using the [Engineering Demo Process](https://about.gitlab.com/handbook/engineering/#engineering-demo-process).  On the tracking issue for each area, each SEG should provide a status update and a link to a video of the current functionality by the end of each week.  We should aim to develop a scorecard to help [grade](https://about.gitlab.com/handbook/engineering/#grading) our progress against each of the key features we are trying to develop, and include this in our weekly update.

We do this in order to ensure alignment with impacted teams at GitLab, to provide opportunties for [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) from interested GitLab team members, community members, and users, and finally to demonstrate [Results](https://about.gitlab.com/handbook/values/#results).

The key points to cover in each of our weekly updates are:

* *What progress have we made?* Each week we should be moving closer to our goal of having a [Viable](https://about.gitlab.com/direction/maturity/) product or feature.
* *What have we learnt?* Either through technical research, experiementation, or feedback, do we better understand the problem we are trying to solve?
* *What new decisions do we need to make?* Have we identified any new opportunities or options that need wider consultation to help address?
* *What is our next focus?* Our next demo is in a week - what do we want to showcase?

When uploading a video to our YouTube channel, we should:

* Make sure they are public
* Use a consistent naming convention - "Incubation Engineering <Section> - Weekly Demo <Long Date> - <Optional Topic>"
* Add the video to the Incubation Engineering playlist, and the playlist dedicated to each section

#### Current issues and updates

*YouTube Playlists*

* [Latest Updates](https://www.youtube.com/playlist?list=PL05JrBw4t0KrQQ6BmQGY0Ji-vBaohNOlW)
* [All Videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KqqLZP0Jue3w2hz2PR6XTHi)

*DevOps for Mobile*

* [Weekly Summary](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/7)
* [Video Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KoVEdembEIySgiciCuZj7Zl)

*APM*

* [Weekly Summary](https://gitlab.com/gitlab-org/incubation-engineering/apm/apm/-/issues/1)
* [Video Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq5pZ8n6vvINQwXeyNa4vbD)

*MLOps*

* [Weekly Summary](https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta/-/issues/16)
* [Video Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpC6-JQy8lY4tNAZKXBaM_-)

*Five Minute Production*

* [Weekly Summary](https://gitlab.com/gitlab-org/incubation-engineering/five-minute-production/meta/-/issues/7)
* [Video Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Krf0LZbfg80yo08DW1c3C36)

## OKRs

The Incubation Engineering department commits to and tracks quarterly [Objectives and Key Results](https://about.gitlab.com/company/okrs/) (OKRs). 

<iframe src="https://app.ally.io/public/YDu9pSHJapFiErx" class="dashboard-embed" height="620" width="100%" style="border:none;"> </iframe>

## Current Focus

### [5 min prod app](/handbook/engineering/incubation/5-min-production/)

The 5 Minute Production App Group will look at our developer onboarding experience and how to make it easier for Web App developers to quickly get their application deployed to Production, and to have the re-assurance that it can scale when needed.

### [DevOps for Mobile Apps](/handbook/engineering/incubation/devops-for-mobile/)

The goal of the DevOps for Mobile Apps SEG is to improve the experience for Developers targeting mobile platforms by providing targeted CI/CD capabilities and workflows that improve the experience of provisioning and deploying mobile apps.

### [DevOps for Salesforce](/handbook/engineering/incubation/devops-for-salesforce/)

The goal of the DevOps for Salesforce SEG is to integrate DevOps capabilities for our customers that develop Salesforce applications.

### [Jamstack](/handbook/engineering/incubation/jamstack/)

The Jamstack group aims to enable Front End developers to simplify their toolkit by using GitLab to manage, build, and deploy externally-facing, static websites using the Jamstack architecture.

### [MLOps](/handbook/engineering/incubation/mlops/)

The MLOps Group will be focused on enabling data teams to build, test, and deploy their machine learning models. This will be net new functionality within GitLab and will bridge the gap between DataOps teams and ML/AI within production. MLOps will provide tuning, testing, and deployment of machine learning models, including version control and partial rollout and rollback.

### [Monitor APM](/handbook/engineering/incubation/monitor-apm/)

The Monitor APM Group aims to integrate monitoring and observability into our DevOps Platform in order to provide a convenient and cost effective solution that allows our customers to monitor the state of their applications, understand how changes they make can impact their applications performance characteristics and give them the tools to resolve issues that arise.

This group is split into three APM's that will focus on each part of the architecture:

**Monitor APM:Agent**

Collecting logs and traces from production systems.

**Monitor APM:Storage**

Storing and querying APM data.

**Monitor APM:Visualisation**

Charts and tables of APM data, with UX for querying and drilling down.

### [No-Code / Low-Code](/handbook/engineering/incubation/no-code/)

The No-Code / Low-Code SEG will integrate with popular low/no-code platforms as well as provide a low code experience inside of GitLab.

### [Real-time Editing of Issue Descriptions (REID)](/handbook/engineering/incubation/real-time-collaboration/)

The Real-time Editing of Issue Descriptions (REID) is gathering feedback on what merge requests and code review in GitLab may be in the future and how we can significantly decrease the cycle time, increase the efficiency of code review, and create better ways of collaborating through real-time experiences.

### [Server Runtime](/handbook/engineering/incubation/server-runtime/)

The Server Runtime SEG will explore how we can increase customer usage and company revenue by delivering a cloud hosted development environment natively within GitLab.com.

### [Service Desk](/handbook/engineering/incubation/service-desk/)

The Service Desk SEG will improve our existing Service Desk offering by integrate support requests into the standard GitLab workflow.


### Backlog

We use the [RICE Framework](https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework) for prioritization of ideas.  

<table id="objectives-table-bizops">
  <tr>
    <th class="text-center">
        <h5>Reach</h5>
    </th>
    <th class="text-center">
        <h5>Impact</h5>
    </th>
    <th class="text-center">
        <h5>Confidence</h5>
    </th>
  </tr>
  <tr>
    <td>How many of our users, prospects, or customers will benefit from this feature.</td>
    <td>Revenue, risk, or cost benefits to GitLab and our customers.</td>
    <td>How well we understand the customer problem and our proposed solution.</td>
  </tr>
  <tr>
    <td>
    <ul>
    <li>10.0 = Impacts the <strong>Vast</strong> majority (~80% or greater)</li>
    <li>6.0 = Impacts a <strong>Large</strong> percentage (~50% to ~80%)</li>
    <li>3.0 = <strong>Significant</strong> reach (~25% to ~50%)</li>
    <li>1.5 = <strong>Small</strong> reach (~5% to ~25%)</li>
    <li>0.5 = <strong>Minimal</strong> reach (Less than ~5%)</li>
    </ul>
    </td>
    <td>
    <ul>
    <li>3 = <strong>Massive</strong></li>
    <li>2 = <strong>High</strong></li>
    <li>1 = <strong>Medium</strong></li>
    <li>0.5 = <strong>Low</strong></li>
    <li>0.25 = <strong>Minimal</strong></li>
    </ul>
    </td>
    <td>
    <ul>
    <li>2 = <strong>High</strong></li>
    <li>1 = <strong>Medium</strong></li>
    <li>0.5 = <strong>Low</strong></li>
    </ul>
    </td>
  </tr>
  <tr>
  <td colspan="3" class="text-center"><h5>Effort</h5></td>
  </tr>
  <tr>
  <td colspan="3">How many months before we can launch an MVP to gather feedback (1 - 3).</td>
  </tr>
  </table>

The total score is calculated by **(Reach x Impact x Confidence) / Effort**

The following table is driven by the relevant RICE labels on the issues themselves.  

If you would like to propose a Single Engineer Group please reach to our [VP of Incubation Engineering](https://gitlab.com/bmarnane).

<%= partial("direction/seg-table", :locals => { :label => "SingleEngineerGroups" }) %>

## Ask Me Anything (AMA)

An AMA was held in May 2021.  

* [Agenda](https://docs.google.com/document/d/1g-xs6kGtWRflkZv4o2LNhqIADpMGzCL7kYd3SqKHkEk/edit#)
* [Video](https://youtu.be/Kb_6GqzHsng)

## Key Performance Indicators

Incubation Engineering [Key Performance Indicators](https://about.gitlab.com/handbook/engineering/incubation/performance-indicators/#incubation-engineering-department-mr-rate)

## Management Issue Board

[Incubation Engineering Issues](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/980804?scope=all&utf8=✓&label_name[]=Engineering%20Management&label_name[]=Incubation%20Engineering%20Department)

You can also reach us on Slack at `#incubation-eng` in [Slack](https://gitlab.slack.com/archives/incubation-eng) (GitLab internal)

## Quality & Support Guidelines for Incubation Projects

- Is your feature behind a feature flag? It's preferrable that it is.
- Can your feature be visually labeled as an `incubation` or `experimental` feature without distracting / taking away from the GitLab user experience?
- Error messages  / stack traces / logs may indicate that this is an `incubation` feature.
- Before feature flag roll-out, is the feature documented in sufficient detail? Documentation is the first resource for users and support agents working with the feature.
- Pro-actively reach out and make introductions before feature roll-out / general availability.
